#!/bin/bash

DIR=/workspace/remote-dev
git clone https://gitlab.com/jeff.mair/dotfiles.git "$DIR/dotfiles"
cp "$DIR/dotfiles/.bash_aliases" ~
cp "$DIR/dotfiles/.gitconfig" ~

_exts=(
  ryu1kn.partial-diff
  esbenp.prettier-vscode
  dbaeumer.vscode-eslint
  ms-python.black-formatter
  sleistner.vscode-fileutils
  yutent.file-stat
  GitHub.copilot
  ms-python.isort
  naumovs.color-highlight
)

for _ext in "${_exts[@]}"; do {
   code --install-extension "$_ext";
} done

echo "executed setup.sh" >> /workspace/log.txt